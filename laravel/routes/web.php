<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');

    Route::post('empresas/get_ciudades', 'EmpresasController@getCiudades');
    Route::resource('empresas', 'EmpresasController');

    /* Administración de productos */
    Route::get('empresas/{id}/productos', [
        'uses' => 'ProductosController@index',
        'as' => 'empresas.productos.index'
    ]);

    Route::get('empresas/{id}/productos/create', [
        'uses' => 'ProductosController@create',
        'as' => 'empresas.productos.create'
    ]);
    Route::post('empresas/productos/store', [
        'uses' => 'ProductosController@store',
        'as' => 'empresas.productos.store'
    ]);

    Route::get('empresas/productos/{id}/edit', [
        'uses' => 'ProductosController@edit',
        'as' => 'empresas.productos.edit'
    ]);
    Route::patch('empresas/productos/{id}/update', [
        'uses' => 'ProductosController@update',
        'as' => 'empresas.productos.update'
    ]);

    Route::delete('empresas/productos/{id}', [
        'uses' => 'ProductosController@destroy',
        'as' => 'empresas.productos.destroy'
    ]);
    /* Fin administración de productos */

    /* --------------------------------------------- */

    /* Administración de clientes */
    Route::get('empresas/{id}/clientes', [
        'uses' => 'ClientesController@index',
        'as' => 'empresas.clientes.index'
    ]);

    Route::get('empresas/{id}/clientes/create', [
        'uses' => 'ClientesController@create',
        'as' => 'empresas.clientes.create'
    ]);
    Route::post('empresas/clientes/store', [
        'uses' => 'ClientesController@store',
        'as' => 'empresas.clientes.store'
    ]);

    Route::get('empresas/clientes/{id}/edit', [
        'uses' => 'ClientesController@edit',
        'as' => 'empresas.clientes.edit'
    ]);
    Route::patch('empresas/clientes/{id}/update', [
        'uses' => 'ClientesController@update',
        'as' => 'empresas.clientes.update'
    ]);

    Route::delete('empresas/clientes/{id}', [
        'uses' => 'ClientesController@destroy',
        'as' => 'empresas.clientes.destroy'
    ]);
    /* Fin administración de clientes */

    /* Administración de procesos */
    Route::get('empresas/{id}/procesos', [
        'uses' => 'ProcesosController@index',
        'as' => 'empresas.procesos.index'
    ]);

    Route::get('empresas/{id}/procesos/create', [
        'uses' => 'ProcesosController@create',
        'as' => 'empresas.procesos.create'
    ]);
    Route::post('empresas/procesos/store', [
        'uses' => 'ProcesosController@store',
        'as' => 'empresas.procesos.store'
    ]);

    Route::get('empresas/procesos/{id}/edit', [
        'uses' => 'ProcesosController@edit',
        'as' => 'empresas.procesos.edit'
    ]);
    Route::patch('empresas/procesos/{id}/update', [
        'uses' => 'ProcesosController@update',
        'as' => 'empresas.procesos.update'
    ]);

    Route::delete('empresas/procesos/{id}', [
        'uses' => 'ProcesosController@destroy',
        'as' => 'empresas.procesos.destroy'
    ]);
    /* Fin administración de procesos */

    /* Administración de problemáticas */
    Route::get('empresas/{id}/problematicas', [
        'uses' => 'ProblematicasController@index',
        'as' => 'empresas.problematicas.index'
    ]);

    Route::get('empresas/{id}/problematicas/create', [
        'uses' => 'ProblematicasController@create',
        'as' => 'empresas.problematicas.create'
    ]);
    Route::post('empresas/problematicas/store', [
        'uses' => 'ProblematicasController@store',
        'as' => 'empresas.problematicas.store'
    ]);

    Route::get('empresas/problematicas/{id}/edit', [
        'uses' => 'ProblematicasController@edit',
        'as' => 'empresas.problematicas.edit'
    ]);
    Route::patch('empresas/problematicas/{id}/update', [
        'uses' => 'ProblematicasController@update',
        'as' => 'empresas.problematicas.update'
    ]);

    Route::delete('empresas/problematicas/{id}', [
        'uses' => 'ProblematicasController@destroy',
        'as' => 'empresas.problematicas.destroy'
    ]);
    /* Fin administración de problemáticas */

    /* Administración de proyectos */
    Route::get('empresas/{id}/proyectos', [
        'uses' => 'ProyectosController@index',
        'as' => 'empresas.proyectos.index'
    ]);

    Route::get('empresas/{id}/proyectos/create', [
        'uses' => 'ProyectosController@create',
        'as' => 'empresas.proyectos.create'
    ]);
    Route::post('empresas/proyectos/store', [
        'uses' => 'ProyectosController@store',
        'as' => 'empresas.proyectos.store'
    ]);

    Route::get('empresas/proyectos/{id}/edit', [
        'uses' => 'ProyectosController@edit',
        'as' => 'empresas.proyectos.edit'
    ]);
    Route::patch('empresas/proyectos/{id}/update', [
        'uses' => 'ProyectosController@update',
        'as' => 'empresas.proyectos.update'
    ]);

    Route::delete('empresas/proyectos/{id}', [
        'uses' => 'ProyectosController@destroy',
        'as' => 'empresas.proyectos.destroy'
    ]);

    //Tareas
    Route::get('empresas/proyectos/{id}/tareas', [
        'uses' => 'ProyectosController@tareas',
        'as' => 'empresas.proyectos.tareas.index'
    ]);

    Route::get('empresas/proyectos/tareas/{id}/', [
        'uses' => 'ProyectosController@verTarea',
        'as' => 'empresas.proyectos.tareas.show'
    ]);

    Route::get('empresas/proyectos/{id}/tareas/create', [
        'uses' => 'ProyectosController@agregarTarea',
        'as' => 'empresas.proyectos.tareas.create'
    ]);
    Route::post('empresas/proyectos/tareas/store', [
        'uses' => 'ProyectosController@guardarTarea',
        'as' => 'empresas.proyectos.tareas.store'
    ]);

    Route::get('empresas/proyectos/tareas/{id}/edit', [
        'uses' => 'ProyectosController@editarTarea',
        'as' => 'empresas.proyectos.tareas.edit'
    ]);

    Route::patch('empresas/proyectos/tareas/{id}/update', [
        'uses' => 'ProyectosController@actualizarTarea',
        'as' => 'empresas.proyectos.tareas.update'
    ]);

    Route::delete('empresas/proyectos/tareas/{id}/', [
        'uses' => 'ProyectosController@eliminarTarea',
        'as' => 'empresas.proyectos.tareas.delete'
    ]);

    //Objetivos
    Route::get('empresas/proyectos/{id}/objetivos/create', [
        'uses' => 'ProyectosController@agregarObjetivo',
        'as' => 'empresas.proyectos.objetivos.create'
    ]);
    Route::post('empresas/proyectos/objetivos/store', [
        'uses' => 'ProyectosController@guardarObjetivo',
        'as' => 'empresas.proyectos.objetivos.store'
    ]);

    Route::get('empresas/proyectos/objetivos/{id}/edit', [
        'uses' => 'ProyectosController@editarObjetivo',
        'as' => 'empresas.proyectos.objetivos.edit'
    ]);

    Route::patch('empresas/proyectos/objetivos/{id}/update', [
        'uses' => 'ProyectosController@actualizarObjetivo',
        'as' => 'empresas.proyectos.objetivos.update'
    ]);

    Route::delete('empresas/proyectos/objetivos/{id}/', [
        'uses' => 'ProyectosController@eliminarObjetivo',
        'as' => 'empresas.proyectos.objetivos.destroy'
    ]);

    //Seguimiento
    Route::get('empresas/proyectos/{id}/seguimiento', [
        'uses' => 'ProyectosController@seguimiento',
        'as' => 'empresas.proyectos.objetivos.seguimiento.index'
    ]);
    Route::get('empresas/proyectos/objetivos/{id}/seguimiento/create', [
        'uses' => 'ProyectosController@agregarSeguimiento',
        'as' => 'empresas.proyectos.objetivos.seguimiento.create'
    ]);
    Route::post('empresas/proyectos/objetivos/seguimiento/store', [
        'uses' => 'ProyectosController@guardarSeguimiento',
        'as' => 'empresas.proyectos.objetivos.seguimiento.store'
    ]);

    Route::get('empresas/proyectos/objetivos/seguimiento/{id}/edit', [
        'uses' => 'ProyectosController@editarSeguimiento',
        'as' => 'empresas.proyectos.objetivos.seguimiento.edit'
    ]);

    Route::patch('empresas/proyectos/objetivos/seguimiento/{id}/update', [
        'uses' => 'ProyectosController@actualizarSeguimiento',
        'as' => 'empresas.proyectos.objetivos.seguimiento.update'
    ]);

    Route::delete('empresas/proyectos/objetivos/seguimiento/{id}/', [
        'uses' => 'ProyectosController@eliminarSeguimiento',
        'as' => 'empresas.proyectos.objetivos.seguimiento.destroy'
    ]);

    /* Fin administración de proyectos */
});