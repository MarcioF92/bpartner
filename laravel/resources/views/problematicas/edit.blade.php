@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar problemática</h1>
        <p>{{ $empresa->nombre }}</p>
    </div>
    {!! Form::model($problematica, ['route' => ['empresas.problematicas.update', $problematica->id], 'method' => 'PATCH', 'role' => 'form', 'files' => true]) !!}
        @include('layouts.alerts')
        @include('problematicas.fields')
    {!! Form::close() !!}
@endsection