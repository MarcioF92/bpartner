@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Problemáticas</h2>
        <p>Listando problemáticas de: <strong>{{ $empresa->nombre }}</strong> - <a href="{{ route('empresas.index') }}">Volver a empresas</a></p>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.problematicas.create', $empresa->id) }}"><i class="material-icons">add</i> Nueva problemática</a>
        <table class="striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($empresa->problematicas as $problematica)
                <tr>
                    <td>{{ $problematica->nombre }}</td>
                    <td>{{ $problematica->descripcion }}</td>

                    <td>
                        <a href="{{ route('empresas.problematicas.edit', $problematica->id) }}" class="edit"><i class="material-icons">edit</i></a>
                        <a href="{{ route('empresas.problematicas.destroy', $problematica->id) }}" class="remove"><i class="material-icons">delete</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection