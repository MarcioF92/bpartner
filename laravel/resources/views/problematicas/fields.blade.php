<div id="problematicas" class="row">
    {{ Form::hidden('empresa_id', $empresa->id, ['class' => 'empresa_id']) }}

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('nombre', $problematica->nombre, ['class' => 'validate nombre_proceso']) }}
            <label for="nombre">Nombre</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::textarea('descripcion', $problematica->descripcion, ['class' => 'materialize-textarea descripcion_proceso']) }}
            <label for="nombre">Descripción</label>
        </div>
    </div>

    <div class="row">
        <div class="col s12">
            <h5>Ejes</h5>

            @foreach($ejes as $eje)
                <p>
                    <input id="eje_{{ $eje->slug }}" name="ejes[]" type="checkbox" value="{{ $eje->id }}"
                    @if($problematica->ejes()->find($eje->id)) checked @endif />
                    <label for="eje_{{ $eje->slug }}">{{ $eje->nombre }}</label>
                </p>
            @endforeach
        </div>
    </div>

    <div class="row">
        <div id="caracteristicas" class="col s12">
            <h5>Datos</h5>
            <a id="agregar-caracteristica" class="waves-effect waves-light"><i class="material-icons right">add</i></a>
            <div id="lista-caracteristicas" class="col s12">
                @foreach($problematica->datos as $dato)
                    <div class="row">
                        <div class="input-field col s5">
                            {{ Form::text('clave_dato[]', $dato->clave, ['class' => 'validate']) }}
                            <label for="clave_dato">Nombre</label>
                        </div>
                        <div class="input-field col s5">
                            {{ Form::text('valor_dato[]', $dato->valor, ['class' => 'validate']) }}
                            <label for="valor_dato">Valor</label>
                        </div>
                        <div class="col s2">
                            <a class="waves-effect waves-light eliminar-caracteristica"><i class="material-icons">delete</i></a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col s12">
        <h4>Diagrama de Ishikawa</h4>
        <div class="col s12">
            <div class="file-field input-field">
                <div class="btn">
                    <span>Archivo</span>
                    {{ Form::file('ishikawa') }}
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        @if(isset($problematica->ishikawa))
            <div class="col s12 center-align">
                <img src="{{ asset($problematica->ishikawa) }}" />
            </div>
        @endif
    </div>

    <div class="col s12">
        <h4>5 Por qué</h4>
        @foreach($problematica->getPorquesInOrder() as $porque)
            <div class="input-field col s12">
                {{ Form::text('porques_preguntas[]', $porque['pregunta'], ['class' => 'validate']) }}
                <label for="porques_preguntas">Pregunta {{ $loop->index + 1 }}</label>
            </div>
            <div class="input-field col s12">
                {{ Form::textarea('porques_respuestas[]', $porque['respuesta'], ['class' => 'validate', 'placeholder' => 'Respuesta '.($loop->index + 1)]) }}
            </div>
        @endforeach
    </div>

    <div class="row">
        <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.problematicas.index', $empresa->id) }}">Cancelar</a>
    </div>
</div>