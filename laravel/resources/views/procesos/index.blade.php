@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Procesos</h2>
        <p>Listando procesos de: <strong>{{ $empresa->nombre }}</strong> - <a href="{{ route('empresas.index') }}">Volver a empresas</a></p>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.procesos.create', $empresa->id) }}"><i class="material-icons">add</i> Nuevo proceso</a>
        <table class="striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($empresa->procesos as $proceso)
                <tr>
                    <td>{{ $proceso->nombre }}</td>
                    <td>{{ $proceso->descripcion }}</td>

                    <td>
                        <a href="{{ route('empresas.procesos.edit', $proceso->id) }}" class="edit"><i class="material-icons">edit</i></a>
                        <a href="{{ route('empresas.procesos.destroy', $proceso->id) }}" class="remove"><i class="material-icons">delete</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection