@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar proceso</h1>
        <p>{{ $empresa->nombre }}</p>
    </div>
    {!! Form::model($proceso, ['route' => ['empresas.procesos.update', $proceso->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('procesos.fields')
    {!! Form::close() !!}
@endsection