@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Agregar proceso</h1>
        <p>{{ $empresa->nombre }}</p>
    </div>
    {!! Form::model($proceso, ['route' => 'empresas.procesos.store', 'method' => 'POST', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('procesos.fields')
    {!! Form::close() !!}
@endsection
