@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/jquery.flowchart.min.css') }}" />
@endsection
<div id="procesos" class="row">
    {{ Form::hidden('empresa_id', $empresa->id, ['class' => 'empresa_id']) }}


    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('nombre', $proceso->nombre, ['class' => 'validate nombre_proceso']) }}
            <label for="nombre">Nombre</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::textarea('descripcion', $proceso->descripcion, ['class' => 'materialize-textarea descripcion_proceso']) }}
            <label for="nombre">Descripción</label>
        </div>
    </div>

    <div class="row">
        <div id="flowchart-actions" class="col s12">

            <div class="row">
                <div class="col s12">
                    <h5>Agregar operador</h5>
                </div>
                <div class="input-field col s12">
                    {{ Form::text('nombre_operador', null, ['class' => 'nuevo_operador_nombre']) }}
                    <label for="nombre_operador">Nombre de operador</label>
                </div>
                <div class="col s12">
                    <p>Tipo de operador</p>
                    <div class="col s3">
                        <input class="with-gap" name="tipo_operador" type="radio" id="inicio" value="inicio" checked />
                        <label for="inicio">Inicio</label>
                        <div class="salidas_inicio">
                            <div class="input-field col s12">
                                {{ Form::number('cantidad_salidas_inicio', 1, ['class' => 'cantidad_salidas_inicio', 'step' => 1, 'min' => 1]) }}
                                <label for="cantidad_salidas">Cantidad de salidas</label>
                            </div>
                        </div>
                    </div>
                    <div class="col s3">
                        <input class="with-gap" name="tipo_operador" type="radio" id="final" value="final"  />
                        <label for="final">Final</label>
                        <div class="entradas_final">
                            <div class="input-field col s12">
                                {{ Form::number('cantidad_entradas_final', 1, ['class' => 'cantidad_entradas_final', 'step' => 1, 'min' => 1]) }}
                                <label for="cantidad_salidas">Cantidad de entradas</label>
                            </div>
                        </div>
                    </div>
                    <div class="col s3">
                        <input class="with-gap" name="tipo_operador" type="radio" id="accion" value="accion"  />
                        <label for="accion">Acción o proceso</label>
                        <div class="entradas_salidas">
                            <div class="input-field col s12">
                                {{ Form::number('cantidad_entradas', 1, ['class' => 'cantidad_entradas', 'step' => 1, 'min' => 1]) }}
                                <label for="cantidad_entradas">Cantidad de entradas</label>
                            </div>
                            <div class="input-field col s12">
                                {{ Form::number('cantidad_salidas', 1, ['class' => 'cantidad_salidas', 'step' => 1, 'min' => 1]) }}
                                <label for="cantidad_salidas">Cantidad de salidas</label>
                            </div>
                        </div>
                    </div>
                    <div class="col s3">
                        <input class="with-gap" name="tipo_operador" type="radio" id="decision" value="decision"  />
                        <label for="decision">Decisión</label>
                    </div>
                </div>
                <div class="col s12">
                    <div class="col s12">
                        <a href="#" class="create_operator waves-effect waves-light btn">Agregar</a>
                        <button class="delete_selected_button">Eliminar seleccionado</button>
                    </div>
                    <div class="col s12">
                        <br>
                        <a href="#" class="edit_operator waves-effect waves-light btn">Editar seleccionado</a>
                    </div>
                </div>
            </div>
        </div>

        <div id="chart_container">
            <div class="flowchart-example-container" id="flowchart" style="height: 100%;min-height: 500px;border: 1px solid #000;overflow: scroll"></div>
        </div>

        <!-- Cambiar datos de operador -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h5>Cambiar valor de operador</h5>
                <p>Nombre del operador:<span class="titulo_operador"></span></p>
                <div class="row">
                    <div class="input-field col s12">
                        {{ Form::text('nuevo_valor_operador', null, ['class' => 'validate nuevo_valor_operador']) }}
                        <label for="nombre">Valor operador</label>
                    </div>
                </div>
                <div class="row">
                    <div class="edit_entradas col s6">
                    </div>
                    <div class="edit_salidas col s6">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat actualizar_operador">Aceptar</a>
            </div>
        </div>

    </div>

    <div class="row">
        <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.procesos.index', $empresa->id) }}">Cancelar</a>
    </div>
</div>

@section('custom-js')
    <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('js/jquery.flowchart.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $("#modal1").modal();

            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var baseUrl = $('meta[name="base-url"]').attr('content');

            var procesos = $("#procesos");
            if (procesos.length) {

                @if(!$proceso->id)
                    var data = {};
                @else
                    var data = {!! $proceso->proceso !!}
                @endif

                // Apply the plugin on a standard, empty div...
                var flowchart = $('#flowchart');
                flowchart.flowchart({
                    data: data
                });

                var actions = $("#flowchart-actions");

                actions.on('click', '.delete_selected_button', function(e) {
                    e.preventDefault();
                    flowchart.flowchart('deleteSelected');
                });

                $(document).on('keyup', function(e){
                    e.preventDefault();
                    if(e.keyCode == 46){
                        flowchart.flowchart('deleteSelected');
                    }
                });

                @if(!$proceso->id)
                    var operatorI = 0;
                @else
                    var operatorI = {!! count(get_object_vars(json_decode($proceso->proceso)->operators)) !!};
                @endif

                actions.on('click', '.create_operator', function(e) {
                    e.preventDefault();

                    var nombreOperador = $(".nuevo_operador_nombre").val();
                    var tipoOperador = $('input[name=tipo_operador]:checked').val();

                    var operatorId = 'created_operator_' + operatorI;
                    var operatorData = {
                        top: 60,
                        left: 60,
                        properties: {
                            title: '<u class="titulo_operador">'+nombreOperador + '</u><br>Valor: <span class="valor_operador"></span>',
                            inputs: {},
                            outputs: {}
                        }
                    };

                    switch (tipoOperador){
                        case 'inicio':
                            var salidas = $(".cantidad_salidas_inicio").val();
                            for(x = 1; x <= salidas; x++) {
                                operatorData['properties']['outputs']['output_'+x] = {
                                    label: 'Salida '+x,
                                }
                            }
                            break;

                        case 'final':
                            var entradas = $(".cantidad_entradas_final").val();
                            var x;
                            for(x = 1; x <= entradas; x++) {
                                operatorData['properties']['inputs']['input_'+x] = {
                                    label: 'Entrada '+x,
                                }
                            }
                            break;

                        case 'accion':
                            var entradas = $(".cantidad_entradas").val();
                            var x;
                            for(x = 1; x <= entradas; x++) {
                                operatorData['properties']['inputs']['input_'+x] = {
                                    label: 'Entrada '+x,
                                }
                            }
                            var salidas = $(".cantidad_salidas").val();
                            for(x = 1; x <= salidas; x++) {
                                operatorData['properties']['outputs']['output_'+x] = {
                                    label: 'Salida '+x,
                                }
                            }
                            break;

                        case 'decision':
                            operatorData['properties']['inputs'] = {
                                input_1: {
                                    label: '',
                                }
                            }
                            operatorData['properties']['outputs'] = {
                                verdadero: {
                                    label: 'Verdadero',
                                },
                                falso: {
                                    label: 'Falso',
                                }
                            }
                            break;
                    }

                    operatorI++;

                    flowchart.flowchart('createOperator', operatorId, operatorData);

                    $(".nuevo_operador_nombre").val('');
                    $(".cantidad_entradas").val(1);
                    $(".cantidad_salidas").val(1);
                    $(".cantidad_salidas_inicio").val(1);
                    $(".cantidad_entradas_final").val(1);

                    $(".show-code").click(function(e){
                        var data = flowchart.flowchart('getData');
                        $(".code").html(JSON.stringify(data, null, 2));
                    });
                });


                /* Actualizar operador */
                var selected;
                var data;
                var operator;

                var titulo;

                actions.on('click', '.edit_operator', function(e){
                    e.preventDefault();

                    $(".valores_entradas").html('');
                    $(".valores_salidas").html('');

                    selected = flowchart.flowchart('getSelectedOperatorId');
                    data = flowchart.flowchart('getData');
                    operator = data['operators'][selected];

                    titulo = operator['properties']['title'].split('<br>')[0];

                    $("#modal1 .titulo_operador").html(titulo);

                    var entradas = operator['properties']['inputs'];
                    var salidas = operator['properties']['outputs'];

                    $.each(entradas, function(index, value){
                        var html = '<div class="input-field col s12">';
                        html += '<input type="text" value="'+value['label']+'" class="validate edit_entrada" data-id="'+index+'">';
                        html += '<label for="entradas['+index+']">Nombre entrada</label>';
                        html += '</div>';
                        $("#modal1 .edit_entradas").append(html);
                    });

                    $.each(salidas, function(index, value){
                        var html = '<div class="input-field col s12">';
                        html += '<input type="text" value="'+value['label']+'" class="validate edit_salida" data-id="'+index+'">';
                        html += '<label for="entradas['+index+']">Nombre salida</label>';
                        html += '</div>';
                        $("#modal1 .edit_salidas").append(html);
                    });

                    if(selected != null) {
                        $("#modal1").modal('open');
                    }
                });

                $('.actualizar_operador').on('click', function(e){
                    e.preventDefault();

                    operator['properties']['title'] = titulo + '<br>Valor: <span class="valor_operador">' + $(".nuevo_valor_operador").val()+'</span>';

                    $(".edit_entrada").each(function(){
                        operator['properties']['inputs'][$(this).data('id')]['label'] = $(this).val();
                    });

                    $(".edit_salida").each(function(){
                        operator['properties']['outputs'][$(this).data('id')]['label'] = $(this).val();
                    });


                    data['operators'][selected] = operator;

                    flowchart.flowchart('setData', data);

                    $(".nuevo_valor_operador").val('');
                    $("#modal1 .edit_entradas").html('');
                    $("#modal1 .edit_salidas").html('');

                })

                $("form").on('submit', function(e){
                    e.preventDefault();
                    e.stopPropagation();

                    var data = flowchart.flowchart('getData');
                    var proceso = JSON.stringify(data, null, 2);

                    @if(!$proceso->id)
                        var url = baseUrl + '/empresas/procesos/store';
                        var method = 'POST';
                    @else
                        var url = baseUrl + '/empresas/procesos/{{ $proceso->id }}/update';
                        var method = 'PATCH';
                    @endif


                    var jqxhr = $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        url: url,
                        method: method,
                        data:{
                            nombre: $('.nombre_proceso').val(),
                            descripcion: $('.descripcion_proceso').val(),
                            empresa_id: $('.empresa_id').val(),
                            proceso: proceso
                        },
                    });

                    jqxhr.done(function (res) {
                        if(res.success){
                            if(res.action == 'refresh') {
                                document.location.href = __root__ + '/empresas/procesos/' + res.id + '/edit?saved=true';
                            }
                            if(res.action == 'toast') {
                                var $toastContent = $('<span>Proceso guardado correctamente</span>').add($('<button class="btn-flat toast-action">x</button>'));
                                Materialize.toast($toastContent, 5000);
                                $('.toast-action').on('click', function () {
                                    Materialize.Toast.removeAll();
                                });
                            }
                        } else {
                            console.log(res.msj)
                        }
                    }).fail(function (response, error, message) {
                        console.log(response.responseText, true);
                    });
                });
            }
            @if(isset($_GET['saved']))
                @if($_GET['saved'])
                    var $toastContent = $('<span>Proceso guardado correctamente</span>').add($('<button class="btn-flat toast-action">x</button>'));
                    Materialize.toast($toastContent, 5000);
                    $('.toast-action').on('click', function () {
                        Materialize.Toast.removeAll();
                    });
                @endif
            @endif
        });
    </script>
@endsection