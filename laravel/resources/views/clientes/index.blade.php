@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Clientes</h2>
        <p>Listando clientes de: <strong>{{ $empresa->nombre }}</strong> - <a href="{{ route('empresas.index') }}">Volver a empresas</a></p>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.clientes.create', $empresa->id) }}"><i class="material-icons">add</i> Agregar cliente</a>
        <table class="striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($empresa->clientes as $cliente)
                <tr>
                    <td>{{ $cliente->nombre }}</td>
                    <td>
                        <a href="{{ route('empresas.clientes.edit', $cliente->id) }}" class="edit"><i class="material-icons">edit</i></a>
                        <a href="{{ route('empresas.clientes.destroy', $cliente->id) }}" class="remove"><i class="material-icons">delete</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection