@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar cliente</h1>
        <p>{{ $empresa->nombre }}</p>
    </div>
    {!! Form::model($cliente, ['route' => ['empresas.clientes.update', $cliente->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('clientes.fields')
    {!! Form::close() !!}
@endsection