<div id="empresas" class="row">
    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('nombre', $empresa->nombre, ['class' => 'validate']) }}
            <label for="nombre">Nombre</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('razon_social', $empresa->razon_social, ['class' => 'validate']) }}
            <label for="nombre">Razón social</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::select('provincia', $provincias, null, ['id' => 'provincia-selector', 'class' => 'validate', 'placeholder' => 'Provincia']) }}
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            @if(!$empresa->id)
                {{ Form::select('ciudad_id', [], null, ['id' => 'ciudad-selector', 'class' => 'validate', 'placeholder' => 'Ciudad']) }}
            @else
                {{ Form::select('ciudad_id', $ciudades, $empresa->ciudad_id, ['id' => 'ciudad-selector', 'class' => 'validate', 'placeholder' => 'Ciudad']) }}
            @endif
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('direccion', $empresa->direccion, ['class' => 'validate']) }}
            <label for="nombre">Dirección</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::textarea('mision', $empresa->mision, ['class' => 'validate materialize-textarea']) }}
            <label for="nombre">Misión</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::textarea('vision', $empresa->vision, ['class' => 'validate materialize-textarea']) }}
            <label for="nombre">Visión</label>
        </div>
    </div>

    <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.index') }}">Cancelar</a>
</div>

@section('custom-js')
    <script>
        $(document).ready(function() {
            @if($empresa->id)
                $('#provincia-selector').val('{{ $empresa->ciudad->provincia->id }}');
                $('#ciudad-selector').val('{{ $empresa->ciudad->id }}');
            @endif

            $('select').material_select();
        });
    </script>
@endsection