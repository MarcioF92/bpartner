@extends('layouts.app')

@section('app-content')
    <div class="md-title"><h1>Agregar empresa</h1></div>
    {!! Form::model($empresa, ['route' => 'empresas.store', 'method' => 'POST', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('empresas.fields')
    {!! Form::close() !!}
@endsection
