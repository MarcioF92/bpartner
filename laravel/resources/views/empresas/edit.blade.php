@extends('layouts.app')

@section('app-content')
    <div class="md-title"><h1>Editar empresa</h1></div>
    {!! Form::model($empresa, ['route' => ['empresas.update', $empresa->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('empresas.fields')
    {!! Form::close() !!}
@endsection