@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2><i class="material-icons">business</i> {{ $empresa->nombre }}</h2>
        <p><a href="{{ route('empresas.edit', ['id' => $empresa->id]) }}" class="edit"><i class="material-icons">edit</i> Editar empresa</a> - <a href="{{ url('empresas') }}" class="edit"><i class="material-icons">undo</i> Volver</a></p>
        <div class="row">
            <p><u>Nombre:</u> {{ $empresa->nombre }}</p>
            <p><u>Razón social:</u> {{ $empresa->razon_social }}</p>
            <p><u>Dirección:</u> {{ $empresa->direccion }}, {{ $empresa->ciudad->nombre }}, {{ $empresa->ciudad->provincia->nombre }}</p>
            <p><u>Misión:</u> {{ $empresa->mision }}</p>
            <p><u>Visión:</u> {{ $empresa->vision }}</p>
        </div>

        <hr>

        <div class="row">
            <h4><i class="material-icons">shopping_cart</i> Productos</h4>
            <ul>
                @foreach($empresa->productos as $producto)
                    <li>{{ $producto->nombre }}</li>
                @endforeach
            </ul>
        </div>

        <hr>

        <div class="row">
            <h4><i class="material-icons">person</i> Clientes</h4>
            <ul>
                @foreach($empresa->clientes as $cliente)
                    <li>{{ $cliente->nombre }}</li>
                @endforeach
            </ul>
        </div>

        <hr>

        <div class="row">
            <h4><i class="material-icons">device_hub</i> Procesos</h4>
            <ul>
                @foreach($empresa->procesos as $proceso)
                    <li>{{ $proceso->nombre }}</li>
                @endforeach
            </ul>
        </div>

        <hr>

        <div class="row">
            <h4><i class="material-icons">error</i> Problemáticas</h4>
            <ul>
                @foreach($empresa->problematicas as $problematica)
                    <li>{{ $problematica->nombre }}</li>
                @endforeach
            </ul>
        </div>

        <hr>

        <div class="row">
            <h4><i class="material-icons">work</i> Proyectos</h4>
            <ul>
                @foreach($empresa->proyectos as $proyecto)
                    <li>{{ $proyecto->nombre }}</li>
                @endforeach
            </ul>
        </div>

    </div>

@endsection