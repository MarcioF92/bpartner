@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Empresas</h2>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.create') }}"><i class="material-icons">add</i> Agregar empresa</a>
        <table class="striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Razón Social</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($empresas as $empresa)
                <tr>
                    <td><a href="{{ route('empresas.show', ['id' => $empresa->id]) }}">{{ $empresa->nombre }}</a></td>
                    <td><a href="{{ route('empresas.show', ['id' => $empresa->id]) }}">{{ $empresa->razon_social }}</a></td>
                    <td>
                        <a href="{{ route('empresas.edit', ['id' => $empresa->id]) }}" class="edit"><i class="material-icons">edit</i></a>
                        <a href="{{ route('empresas.productos.index', ['id' => $empresa->id]) }}" title="Productos"><i class="material-icons">shopping_cart</i></a>
                        <a href="{{ route('empresas.clientes.index', ['id' => $empresa->id]) }}" title="Clientes"><i class="material-icons">person</i></a>
                        <a href="{{ route('empresas.procesos.index', ['id' => $empresa->id]) }}" title="Procesos"><i class="material-icons">device_hub</i></a>
                        <a href="{{ route('empresas.problematicas.index', ['id' => $empresa->id]) }}" title="Problemáticas"><i class="material-icons">error</i></a>
                        <a href="{{ route('empresas.proyectos.index', ['id' => $empresa->id]) }}" title="Proyectos"><i class="material-icons">work</i></a>
                        <a href="{{ route('empresas.destroy', ['id' => $empresa->id]) }}" class="remove"><i class="material-icons">delete</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection