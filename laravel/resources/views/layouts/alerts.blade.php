@if (isset($errors) && count($errors))
    <div id="alerts" class="errors">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif(session()->has('alerts'))
    <div id="alerts" class="errors">
        <ul>
            <li>{{ session()->get('alerts') }}</li>
        </ul>
    </div>
@endif
