@extends('layouts.empty')

@section('content')

    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper">
                <ul class="left col hide-on-med-and-down">
                    <li><a href="{{ url('/') }}">BPartner</a></li>
                    <li><a href="{{ route('empresas.index') }}">Empresas</a></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                    @else

                        <li class="dropdown">
                            <a href="#" class="dropdown-button" href="#!" data-activates="dropdown-user">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul id="dropdown-user" class="dropdown-content">
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </nav>
    </div>

    <div class="container">
        @yield('app-content')
    </div>

@endsection