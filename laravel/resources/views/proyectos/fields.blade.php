<div id="proyectos" class="row">
    {{ Form::hidden('empresa_id', $empresa->id) }}

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('nombre', $proyecto->nombre, ['class' => 'validate']) }}
            <label for="nombre">Nombre</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::textarea('descripcion', $proyecto->descripcion, ['class' => 'materialize-textarea']) }}
            <label for="nombre">Descripción</label>
        </div>
    </div>

    <div class="row">
        <div class="col s12">
            <h5>Problemáticas relacionadas</h5>

            @foreach($empresa->problematicas as $problematica)
                <p>
                    <input id="problematica_{{ $problematica->id }}" name="problematicas[]" type="checkbox" value="{{ $problematica->id }}" @if($proyecto->problematicas()->find($problematica->id)) checked @endif />
                    <label for="problematica_{{ $problematica->id }}">{{ $problematica->nombre }}</label>
                </p>
            @endforeach
        </div>
    </div>

    <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.proyectos.index', $empresa->id) }}">Cancelar</a>
</div>
