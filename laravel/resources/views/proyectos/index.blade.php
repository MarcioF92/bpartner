@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Proyectos</h2>
        <p>Listando proyectos de: <strong>{{ $empresa->nombre }}</strong> - <a href="{{ route('empresas.index') }}">Volver a empresas</a></p>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.proyectos.create', $empresa->id) }}"><i class="material-icons">add</i> Agregar proyecto</a>
        <table class="striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($empresa->proyectos as $proyecto)
                <tr>
                    <td>{{ $proyecto->nombre }}</td>
                    <td>
                        <a href="{{ route('empresas.proyectos.edit', $proyecto->id) }}" class="edit" title="Ediar"><i class="material-icons">edit</i></a>
                        <a href="{{ route('empresas.proyectos.tareas.index', $proyecto->id) }}" title="Tareas"><i class="material-icons">event_note</i></a>
                        <a href="{{ route('empresas.proyectos.objetivos.seguimiento.index', $proyecto->id) }}" title="Avances"><i class="material-icons">trending_up</i></a>
                        <a href="{{ route('empresas.proyectos.destroy', $proyecto->id) }}" class="remove"><i class="material-icons">delete</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection