@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Agregar seguimiento</h1>
        <p>
            <strong>Indicador:</strong> {{ $objetivo->clave }}<br>
            <strong>Meta:</strong> {{ $objetivo->valor }} {{ $objetivo->medida->etiqueta }}
        </p>
    </div>
    {!! Form::model($seguimiento, ['route' => 'empresas.proyectos.objetivos.seguimiento.store', 'method' => 'POST', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('proyectos.seguimiento.fields')
    {!! Form::close() !!}
@endsection
