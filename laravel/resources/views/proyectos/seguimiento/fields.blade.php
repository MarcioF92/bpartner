<div id="proyectos" class="row">
    {{ Form::hidden('objetivo_id', $objetivo->id) }}
    {{ Form::hidden('proyecto_id', $objetivo->proyecto->id) }}

    <div class="row">
        <div class="input-field col s10">
            {{ Form::text('valor', $seguimiento->valor, ['class' => 'validate']) }}
            <label for="valor">Valor</label>
        </div>
        <div class="col s2">
            {{ $objetivo->medida->etiqueta }}
        </div>
    </div>

    <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.proyectos.objetivos.seguimiento.index', $objetivo->empresa_id) }}">Cancelar</a>
</div>