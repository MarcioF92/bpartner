{{ Form::hidden('proyecto_id', $proyecto->id) }}
<div class="row">
    <div class="input-field col s4">
        {{ Form::text('clave', $objetivo->clave, ['class' => 'validate']) }}
        <label for="clave_dato">Nombre</label>
    </div>
    <div class="input-field col s4">
        {{ Form::text('valor', $objetivo->valor, ['class' => 'validate']) }}
        <label for="valor_dato">Valor</label>
    </div>
    <div class="col s1">
        {{ Form::select('medida_id', $medidas, $objetivo->medida_id, ['class' => 'validate']) }}
    </div>
    <div class="col s1">
        <a class="waves-effect waves-light eliminar-objetivo"><i class="material-icons">delete</i></a>
    </div>

</div>
<button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.proyectos.objetivos.seguimiento.index', $proyecto->id) }}">Cancelar</a>
@section('custom-js')
    <script>
        $(document).ready(function() {
            $('select').material_select();
        });
    </script>
@endsection