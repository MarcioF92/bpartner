@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Agregar objetivo</h1>
        <p>
            <strong>Proyecto:</strong> {{ $proyecto->nombre }}
        </p>
    </div>
    {!! Form::model($objetivo, ['route' => 'empresas.proyectos.objetivos.store', 'method' => 'POST', 'role' => 'form']) !!}
    @include('layouts.alerts')
    @include('proyectos.seguimiento.fields-objetivo')
    {!! Form::close() !!}
@endsection
