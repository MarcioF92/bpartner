@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Seguimiento de proyecto</h2>
        <p>Seguimiento de: <strong>{{ $proyecto->nombre }}</strong> - <a href="{{ route('empresas.proyectos.index', ['id' => $proyecto->empresa->id]) }}">Volver a proyectos</a></p>

        <div class="col s12">
            <a href="{{ route('empresas.proyectos.objetivos.create', ['id' => $proyecto->id]) }}">+ Agregar objetivo</a>
        </div>

        @foreach($proyecto->objetivos as $objetivo)
            <div class="col s12">
                <h3>
                    Objetivo {{ $loop->iteration }} -
                    <a href="{{ route('empresas.proyectos.objetivos.edit', $objetivo->id) }}" class="edit" title="Ediar"><i class="material-icons">edit</i></a>
                    <a href="{{ route('empresas.proyectos.objetivos.destroy', $objetivo->id) }}" class="remove"><i class="material-icons">delete</i></a>
                </h3>
                <p><a href="{{ route('empresas.proyectos.objetivos.seguimiento.create', ['id' => $objetivo->id]) }}">+ Agregar seguimiento</a></p>
                <p><strong>Indicador:</strong> {{ $objetivo->clave }}<br>
                    <strong>Meta:</strong> {{ $objetivo->valor }} {{ $objetivo->medida->etiqueta }}</p>
                <table>
                    <tr>
                        <th>Fecha</th>
                        <th>Valor</th>
                        <th>Target</th>
                        <th>Acciones</th>
                    </tr>
                    @foreach($objetivo->seguimiento as $seguimiento)
                        <tr>
                            <td>
                                {{ $seguimiento->created_at }}
                            </td>
                            <td>
                                {{ $seguimiento->valor }} {{ $seguimiento->objetivo->medida->etiqueta }}
                            </td>
                            <td>
                                {{ $seguimiento->objetivo->valor }}
                            </td>
                            <td>
                                <a href="{{ route('empresas.proyectos.objetivos.seguimiento.edit', $seguimiento->id) }}" class="edit"><i class="material-icons">edit</i></a>
                                <a href="{{ route('empresas.proyectos.objetivos.seguimiento.destroy', $seguimiento->id) }}" class="remove"><i class="material-icons">delete</i></a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td>Total hasta la fecha: {{ date('d-m-Y') }}</td>
                        <td>{{ $objetivo->getTotalSeguimiento() }} {{ $objetivo->medida->etiqueta }}</td>
                        <td>
                            Diferencia: {{ $objetivo->valor - $objetivo->getTotalSeguimiento()  }}
                        </td>
                    </tr>
                </table>
                <hr>
            </div>
        @endforeach

    </div>

@endsection