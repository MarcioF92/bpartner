@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar objetivo</h1>
        <p>
            <strong>Proyecto:</strong> {{ $proyecto->nombre }}
        </p>
    </div>
    {!! Form::model($objetivo, ['route' => ['empresas.proyectos.objetivos.update', $objetivo->id], 'method' => 'PATCH', 'role' => 'form']) !!}
    @include('layouts.alerts')
    @include('proyectos.seguimiento.fields-objetivo')
    {!! Form::close() !!}
@endsection