@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar seguimiento</h1>
        <p>
            <strong>Indicador:</strong> {{ $objetivo->clave }}<br>
            <strong>Meta:</strong> {{ $objetivo->valor }} {{ $objetivo->medida->etiqueta }}
        </p>
        <p>Fecha del seguimiento: {{ $seguimiento->created_at }}</p>
    </div>
    {!! Form::model($seguimiento, ['route' => ['empresas.proyectos.objetivos.seguimiento.update', $seguimiento->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('proyectos.seguimiento.fields')
    {!! Form::close() !!}
@endsection