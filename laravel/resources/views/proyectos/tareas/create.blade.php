@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Agregar tarea</h1>
        <p>{{ $proyecto->nombre }}</p>
    </div>
    {!! Form::model($tarea, ['route' => 'empresas.proyectos.tareas.store', 'method' => 'POST', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('proyectos.tareas.fields')
    {!! Form::close() !!}
@endsection
