@extends('layouts.app')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/fullcalendar.min.css') }}" />
@endsection

@section('app-content')

    <div id="empresas" class="row">
        <h2>Tareas</h2>
        <p><strong>{{ $proyecto->nombre }}</strong> - <a href="{{ route('empresas.proyectos.index', ['id' => $proyecto->empresa->id]) }}">Volver a proyectos</a></p>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.proyectos.tareas.create', ['id' => $proyecto->id]) }}"><i class="material-icons">add</i> Agregar tarea</a>
        <div class="row">
            <div id='calendar'></div>
        </div>
    </div>

@endsection

@section('custom-js')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar/locale/es.js') }}"></script>
    <script>
        $(function() {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                defaultView: 'month',
                events: [
                    @foreach($proyecto->tareas as $tarea)
                    {
                        title: '{{ $tarea->nombre }}',
                        start: '{{ $tarea->fecha_comienzo }}',
                        end: '{{ $tarea->fecha_fin }}',
                        url: '{{ url("/empresas/proyectos/tareas/".$tarea->id) }}',
                        @if($tarea->prioridad->slug == "baja")
                            backgroundColor: '#00ff00',
                            borderColor: '#00ff00'
                        @endif
                        @if($tarea->prioridad->slug == "media")
                            backgroundColor: '#0000ff',
                            borderColor: '#0000ff'
                        @endif
                        @if($tarea->prioridad->slug == "alta")
                            backgroundColor: '#5F021F',
                            borderColor: '#5F021F'
                        @endif
                        @if($tarea->prioridad->slug == "urgente")
                            backgroundColor: '#ff0000',
                            borderColor: '#ff0000'
                        @endif
                    }
                    @if(!$loop->last) , @endif
                    @endforeach
                ]
            })

        });
    </script>
@endsection