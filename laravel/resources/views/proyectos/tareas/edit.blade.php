@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar tarea</h1>
        <p>{{ $proyecto->nombre }}</p>
    </div>
    {!! Form::model($tarea, ['route' => ['empresas.proyectos.tareas.update', $tarea->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('proyectos.tareas.fields')
    {!! Form::close() !!}
@endsection