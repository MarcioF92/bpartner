<div id="productos" class="row">
    {{ Form::hidden('proyecto_id', $proyecto->id) }}

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('nombre', $tarea->nombre, ['class' => 'validate']) }}
            <label for="nombre">Nombre</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            {{ Form::textarea('descripcion', $tarea->descripcion, ['class' => 'materialize-textarea']) }}
            <label for="nombre">Descripción</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s6">
            {{ Form::text('fecha_comienzo', $fechaInicio, ['class' => 'datepicker']) }}
            <label for="fecha_comienzo">Fecha comienzo</label>
        </div>
        <div class="input-field col s6">
            {{ Form::text('fecha_fin',$fechaFin, ['class' => 'datepicker']) }}
            <label for="fecha_fin">Fecha fin</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s6">
            <select name="prioridad">
                @foreach($prioridades as $prioridad)
                    <option value="{{ $prioridad->id }}" @if($tarea->prioridad()->find($prioridad->id)) selected @endif>{{ $prioridad->nombre }}</option>
                @endforeach
            </select>
            <label>Prioridad</label>
        </div>
        <div class="input-field col s6">
            <select name="padre" placeholder="Elije una tarea padre">
                <option value="-1" disabled selected>Elejí una tarea padre</option>
                @foreach($tareas as $tareaPadre)
                    <option value="{{ $tareaPadre->id }}" @if($tarea->padre()->find($tareaPadre->id)) selected @endif>{{ $tareaPadre->nombre }}</option>
                @endforeach
            </select>
            <label>Tarea padre</label>
        </div>
    </div>

    <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.proyectos.tareas.index', $proyecto->id) }}">Cancelar</a>
</div>

@section('custom-js')
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $('.datepicker').pickadate({
                selectMonths: true, // Creates a dropdown to control month
                selectYears: 15, // Creates a dropdown of 15 years to control year,
                today: 'Hoy',
                clear: 'Limpiar',
                close: 'Ok',
                closeOnSelect: true, // Close upon selecting a date,
                format: 'dd-mm-yyyy',
                formatSubmit: 'yyyy-mm-dd',
            });
        });
    </script>
@endsection