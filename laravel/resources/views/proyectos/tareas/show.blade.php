@extends('layouts.app')

@section('app-content')

    <div id="tarea" class="row">
        <div class="col m12">
            <h2><i class="material-icons">edit</i> {{ $tarea->nombre }}</h2>
            <p>{{ $tarea->descripcion }}</p>
        </div>
        <div class="col m12">
            <p><i class="material-icons">event_note</i> Fecha de comienzo: {{ $tarea->fecha_comienzo }}</p>
            <p><i class="material-icons">event_note</i> Fecha de fin: {{ $tarea->fecha_fin }}</p>
            <p><i class="material-icons">error</i> Prioridad: {{ $tarea->prioridad->nombre }}</p>
            @if($tarea->padre)
            <p><i class="material-icons">edit</i> Tarea padre:
                {{ optional($tarea->padre)->nombre }}</p>
                @endif
        </div>
        <div class="col m12">
            <a href="{{ route('empresas.proyectos.tareas.edit', ['id' => $tarea->id]) }}">Editar tarea</a> - <a class="remove" data-type="tarea" data-proyecto="{{ $tarea->proyecto_id }}" href="{{ route('empresas.proyectos.tareas.delete', ['id' => $tarea->id]) }}">Eliminar tarea</a>
        </div>
    </div>

@endsection