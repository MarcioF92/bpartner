@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar proyecto</h1>
        <p>{{ $empresa->nombre }}</p>
    </div>
    {!! Form::model($proyecto, ['route' => ['empresas.proyectos.update', $proyecto->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('proyectos.fields')
    {!! Form::close() !!}
@endsection