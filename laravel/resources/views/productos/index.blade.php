@extends('layouts.app')

@section('app-content')

    <div id="empresas" class="row">
        <h2>Productos</h2>
        <p>Listando productos de: <strong>{{ $empresa->nombre }}</strong> - <a href="{{ route('empresas.index') }}">Volver a empresas</a></p>
        <a class="waves-effect waves-light btn" href="{{ route('empresas.productos.create', $empresa->id) }}"><i class="material-icons">add</i> Agregar producto</a>
        <table class="striped">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
            @foreach($empresa->productos as $producto)
                <tr>
                    <td>{{ $producto->nombre }}</td>
                    <td>
                        <a href="{{ route('empresas.productos.edit', $producto->id) }}" class="edit"><i class="material-icons">edit</i></a>
                        <a href="{{ route('empresas.productos.destroy', $producto->id) }}" class="remove"><i class="material-icons">delete</i></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection