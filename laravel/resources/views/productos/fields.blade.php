<div id="productos" class="row">
    {{ Form::hidden('empresa_id', $empresa->id) }}

    <div class="row">
        <div class="input-field col s12">
            {{ Form::text('nombre', $producto->nombre, ['class' => 'validate']) }}
            <label for="nombre">Nombre</label>
        </div>
    </div>

    <button type="submit" class="waves-effect waves-light btn">Guardar</button> - <a href="{{ route('empresas.productos.index', $empresa->id) }}">Cancelar</a>
</div>