@extends('layouts.app')

@section('app-content')
    <div class="md-title">
        <h1>Editar producto</h1>
        <p>{{ $empresa->nombre }}</p>
    </div>
    {!! Form::model($producto, ['route' => ['empresas.productos.update', $producto->id], 'method' => 'PATCH', 'role' => 'form']) !!}
        @include('layouts.alerts')
        @include('productos.fields')
    {!! Form::close() !!}
@endsection