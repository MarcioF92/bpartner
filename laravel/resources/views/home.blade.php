@extends('layouts.app')

@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/fullcalendar.min.css') }}" />
@endsection

@section('app-content')
    <div class="row">
        <div class="col md12">
            <div class="card">
                <div class="card-content">
                    <h2>Próximas tareas</h2>
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
        <div class="col m6">
            <div class="card">
                <div class="card-content">
                    <span class="card-title">Últimas empresas agregadas</span>
                    @foreach($empresas as $empresa)
                        <a href="{{ route('empresas.show', ['id' => $empresa->id]) }}">{{ $empresa->nombre }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-js')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar/locale/es.js') }}"></script>
    <script>
        $(function() {

            // page is now ready, initialize the calendar...

            $('#calendar').fullCalendar({
                defaultView: 'month',
                height: 300,
                events: [
                        @foreach($tareas as $tarea)
                    {
                        title: '{{ $tarea->nombre }}',
                        start: '{{ $tarea->fecha_comienzo }}',
                        end: '{{ $tarea->fecha_fin }}',
                        url: '{{ url("/empresas/proyectos/tareas/".$tarea->id) }}',
                    }
                    @if(!$loop->last) , @endif
                    @endforeach
                ]
            })

        });
    </script>
@endsection