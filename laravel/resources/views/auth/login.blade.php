@extends('layouts.empty')

@section('content')

<div class="row login-form-container">
    <div class="col s12 m4 offset-m4">
        <div class="card blue-grey darken-1">
            <div class="card-content white-text">
                <h2 class="card-title text-center">B-Partner</h2>

                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="input-field col s12">
                        <label for="email">E-Mail</label>
                        <input id="email" type="email" class="validate" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="input-field col s12">
                        <label for="password">Password</label>
                        <input id="password" type="password" class="validate"  name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Ingresar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
