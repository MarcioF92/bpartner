<?php

namespace App\Repo\Readers;

use Illuminate\Support\Facades\Session;

class BaseReader {

    protected $model;

    protected function __construct($model)
    {
        $this->model = $model;
    }

    public function newItem() {
        return new $this->model;
    }

    public function findItem($id) {
        return  $this->model::find($id);
    }

    public function all(){
        return $this->model::all();
    }

    public function allInSite($siteId){
        return $this->model::where('sitio_id', $siteId)->get();
    }

    public function itemList() {
        return $this->model::orderBy('created_at', 'desc')->get();
    }

    public function findBy($campo, $valor, $operator = false, $multiple = false)
    {
        if (!$operator) {
            $model = $this->model::where($campo, $valor);
        } else {
            $model = $this->model::where($campo, $operator, $valor);
        }

        if (!$multiple) {
            return $model->first();
        } else {
            return $model->get();
        }
    }

    public function validarSlug($slug, $tipoId = false){
        $obj = $this->findBy('slug',
            $slug,
            false,
            false,
            Session::get('sitio-activo-backend')->id);
        return $obj != null;
    }
}