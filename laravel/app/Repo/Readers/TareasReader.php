<?php

namespace App\Repo\Readers;

use App\Repo\Entities\Prioridad;

class TareasReader extends BaseReader {

    public function __construct($model = 'App\Repo\Entities\Tarea')
    {
        parent::__construct($model);
    }

    public function getPrioridades(){
        return Prioridad::all();
    }

}