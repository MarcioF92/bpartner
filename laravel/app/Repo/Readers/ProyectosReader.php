<?php

namespace App\Repo\Readers;

use App\Repo\Entities\Medida;
use App\Repo\Entities\Objetivo;
use App\Repo\Entities\Seguimiento;

class ProyectosReader extends BaseReader {

    public function __construct($model = 'App\Repo\Entities\Proyecto')
    {
        parent::__construct($model);
    }

    public function getMedidas(){
        return Medida::pluck('etiqueta', 'id')->all();
    }

    public function getObjetivo($id){
        return Objetivo::find($id);
    }

    public function newSeguimiento($id){
        return new Seguimiento();
    }

    public function getSeguimiento($id){
        return Seguimiento::find($id);
    }

}