<?php

namespace App\Repo\Readers;

use App\Repo\Entities\Provincia;
use App\Repo\Entities\Ciudad;


class EmpresasReader extends BaseReader {

    public function __construct($model = 'App\Repo\Entities\Empresa')
    {
        parent::__construct($model);
    }

    public function getProvincias(){
        return Provincia::pluck('nombre', 'id')->all();
    }

    public function getCiudades($provinciaId){
        return Provincia::find($provinciaId)->ciudades()->pluck('nombre', 'id')->all();
    }

}