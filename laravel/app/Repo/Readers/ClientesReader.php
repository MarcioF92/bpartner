<?php

namespace App\Repo\Readers;

use App\Repo\Entities\Provincia;

class ClientesReader extends BaseReader {

    public function __construct($model = 'App\Repo\Entities\Cliente')
    {
        parent::__construct($model);
    }

    public function getProvincias(){
        return Provincia::pluck('nombre', 'id')->all();
    }

    public function getCiudades($provinciaId){
        return Provincia::find($provinciaId)->ciudades()->pluck('nombre', 'id')->all();
    }

}