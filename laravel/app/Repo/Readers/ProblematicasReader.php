<?php

namespace App\Repo\Readers;

use App\Repo\Entities\Eje;

class ProblematicasReader extends BaseReader {

    public function __construct($model = 'App\Repo\Entities\Problematica')
    {
        parent::__construct($model);
    }

    public function getEjes($id = false){
        if(!$id)
            return Eje::all();
        $problematica = $this->model::find($id);
        return $problematica->ejes;
    }

}