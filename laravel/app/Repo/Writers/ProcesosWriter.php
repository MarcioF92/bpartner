<?php

namespace App\Repo\Writers;

use App\Repo\Entities\Proceso;

class ProcesosWriter extends BaseWriter {

    public function save() {
        \DB::beginTransaction();
        try {
            parent::save();
            $id = $this->entity->id;
            \DB::commit();
            return [
                'success' => true,
                'id' => $id
            ];
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }

    public function destroy() {
        return parent::destroy();
    }

}