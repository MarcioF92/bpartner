<?php

namespace App\Repo\Writers;

use App\Repo\Entities\Cliente;

class ClientesWriter extends BaseWriter {

    public function save() {
        \DB::beginTransaction();
        try {
            parent::save();
            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }

    public function destroy() {
        return parent::destroy();
    }

}