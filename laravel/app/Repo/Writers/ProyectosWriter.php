<?php

namespace App\Repo\Writers;

use App\Repo\Entities\Objetivo;
use App\Repo\Entities\Prioridad;
use App\Repo\Entities\Problematica;
use App\Repo\Entities\Proyecto;
use App\Repo\Entities\Seguimiento;
use App\Repo\Entities\Tarea;

class ProyectosWriter extends BaseWriter {

    public function save() {
        \DB::beginTransaction();
        try {

            $this->entity->empresa_id = $this->request->input('empresa_id');
            $this->entity->nombre = $this->request->input('nombre');
            $this->entity->descripcion = $this->request->input('descripcion');

            $this->entity->save();

            $this->entity->problematicas()->detach();
            foreach ($this->request->input('problematicas', []) as $problematicaId){
                $problematica = Problematica::find($problematicaId);
                $this->entity->problematicas()->save($problematica);
            }

            $this->entity->objetivos()->delete();
            foreach ($this->request->input('clave_objetivo', []) as $key => $claveObjetivo){
                $this->entity->objetivos()->create([
                    'clave' => $claveObjetivo,
                    'valor' => $this->request->input('valor_objetivo')[$key],
                    'medida_id' => $this->request->input('medida_objetivo')[$key],
                ]);
            }

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }

    public function guardarTarea($id = false){
        \DB::beginTransaction();
        try {

            if(!$id) {
                $tarea = new Tarea();
            } else {
                $tarea = Tarea::find($id);
                $tarea->proyecto()->dissociate($this->entity);
            }

            $tarea->nombre = $this->request->input('nombre');
            $tarea->descripcion = $this->request->input('descripcion');
            $tarea->fecha_comienzo = $this->request->input('fecha_comienzo_submit');
            $tarea->fecha_fin = $this->request->input('fecha_fin_submit');

            $tarea->save();

            $tarea->prioridad()->dissociate();
            $prioridad = Prioridad::find($this->request->input('prioridad'));
            $tarea->prioridad()->associate($prioridad);

            $tarea->padre()->dissociate();
            $padre = Tarea::find($this->request->input('padre'));
            if($padre != null)
                $tarea->padre()->associate($padre);

            $tarea->save();

            $this->entity->tareas()->save($tarea);

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }
    public function eliminarTarea($id){
        \DB::beginTransaction();
        try {

            $tarea = Tarea::find($id);

            foreach ($tarea->hijas as $hija){
                $hija->padre()->dissociate();
            }

            $tarea->delete();

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }

    public function guardarObjetivo($id = false){
        \DB::beginTransaction();
        try {

            if(!$id) {
                $objetivo = new Objetivo();
                $objetivo->proyecto_id = $this->request->input('proyecto_id');
                $objetivo->save();
            } else {
                $objetivo = $this->entity->objetivos()->find($id);
            }

            $objetivo->clave = $this->request->input('clave');
            $objetivo->valor = $this->request->input('valor');
            $objetivo->medida_id = $this->request->input('medida_id');

            $objetivo->save();

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }
    public function eliminarObjetivo($id){
        \DB::beginTransaction();
        try {

            $objetivo = Objetivo::find($id);

            foreach ($objetivo->seguimiento as $seguimiento){
                $seguimiento->delete();
            }

            $objetivo->delete();

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }

    public function guardarSeguimiento($id = false){
        \DB::beginTransaction();
        try {

            if(!$id) {
                $seguimiento = new Seguimiento();
            } else {
                $seguimiento = Seguimiento::find($id);
            }

            $seguimiento->valor = $this->request->input('valor');
            $seguimiento->objetivo_id = $this->request->input('objetivo_id');

            $seguimiento->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }
    public function eliminarSeguimiento($id){
        \DB::beginTransaction();
        try {

            $seguimiento = Seguimiento::find($id);

            $seguimiento->delete();

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }



    public function destroy() {
        $this->entity->problematicas()->detach();
        $this->entity->tareas()->delete();
        return parent::destroy();
    }

}