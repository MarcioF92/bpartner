<?php

namespace App\Repo\Writers;

use App\Repo\Models\Medio;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\MessageBag;

class BaseWriter {

    protected $entity;
    protected $request;
    protected $errors;
    protected $alerts;

    public function __construct($entity, $request) {
        $this->entity = $entity;
        $this->request = $request;
        $this->alerts = new MessageBag;
        $this->errors = new MessageBag;
    }

    public function entity() {
        return $this->entity;
    }

    public function save() {
        $this->entity->fill($this->request->all());
        $this->entity->save();
        $this->entity->touch();
        return true;
    }

    public function ajaxSave() {
        $this->entity->fill($this->request->all());
        $this->entity->save();
        return $this->entity;
    }

    public function addError($key, $value) {
        $this->errors->add($key, $value);
    }

    public function addAlert($key, $value) {
        $this->alerts->add($key, $value);
    }

    public function getErrors() {
        return $this->errors;
    }

    public function getAlerts() {
        return $this->alerts;
    }

    private function messagesAsString($msgBag, $separator) {
        $result = '';
        $sep = '';
        foreach($msgBag->all() as $msg) {
            $result .= $sep . $msg;
            $sep = $separator;
        }
        return $result;
    }

    public function getErrorsToString($separator = '<br>') {
        return $this->messagesAsString($this->errors, $separator);
    }

    public function getAlertsToString($separator = '<br>') {
        return $this->messagesAsString($this->alerts, $separator);
    }

    public function destroy() {
        if (!count($this->errors->all())) {
            $this->entity->delete();
            return true;
        } return false;
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

    public function stringToBool($string){
        if(strtolower($string) == 'false')
            return false;
        return true;
    }

    public function uploadMedium($archivo){
        if ($archivo->isValid()) {

            $folder = '/public/images';

            $nombreArchivo = pathinfo($archivo->getClientOriginalName(), PATHINFO_FILENAME) . '.' . $archivo->getClientOriginalExtension();

            $path = $archivo->storeAs($folder, $nombreArchivo);

            return $path;
        }
    }

}
