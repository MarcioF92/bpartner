<?php

namespace App\Repo\Writers;

use App\Repo\Entities\Dato;
use App\Repo\Entities\Eje;
use App\Repo\Entities\Problematica;

class ProblematicasWriter extends BaseWriter {

    public function save() {
        \DB::beginTransaction();
        try {
            $this->entity->empresa_id = $this->request->input('empresa_id');
            $this->entity->nombre = $this->request->input('nombre');
            $this->entity->descripcion = $this->request->input('descripcion');

            $this->entity->save();

            $this->entity->ejes()->detach();
            foreach ($this->request->input('ejes', []) as $ejeId){
                $eje = Eje::find($ejeId);
                $this->entity->ejes()->save($eje);
            }

            $this->entity->datos()->delete();
            $valoresDatos = $this->request->input('valor_dato', []);
            foreach ($this->request->input('clave_dato', []) as $item => $clave){
                $this->entity->datos()->create([
                    'clave' => $clave,
                    'valor' => $valoresDatos[$item]
                ]);
            }

            $this->entity->porques()->delete();
            $valoresRespuestas = $this->request->input('porques_respuestas', []);
            foreach ($this->request->input('porques_preguntas', []) as $item => $clave){
                $this->entity->porques()->create([
                    'pregunta' => $clave,
                    'respuesta' => $valoresRespuestas[$item],
                    'orden' => $item
                ]);
            }

            if($this->request->file('ishikawa')) {
                $this->entity->ishikawa = $this->uploadMedium($this->request->file('ishikawa'));
            }

            $this->entity->save();

            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError('db', 'No se pudo guardar el registro: '.$e->getMessage());
            return false;
        }
    }

    public function destroy() {
        $this->entity->ejes()->detach();
        $this->entity->datos()->delete();
        return parent::destroy();
    }

}