<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{

    protected $table = 'empresas';

    protected $guarded = ['id', 'provincia'];

    public function ciudad(){
        return $this->belongsTo('App\Repo\Entities\Ciudad');
    }

    public function productos(){
        return $this->hasMany('App\Repo\Entities\Producto', 'empresa_id');
    }

    public function clientes(){
        return $this->hasMany('App\Repo\Entities\Cliente', 'empresa_id');
    }

    public function procesos(){
        return $this->hasMany('App\Repo\Entities\Proceso', 'empresa_id');
    }

    public function problematicas(){
        return $this->hasMany('App\Repo\Entities\Problematica', 'empresa_id');
    }

    public function proyectos(){
        return $this->hasMany('App\Repo\Entities\Proyecto', 'empresa_id');
    }

}