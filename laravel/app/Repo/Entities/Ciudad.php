<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{

    protected $table = 'ciudades';

    protected $guarded = ['id'];

    public function provincia(){
        return $this->belongsTo('App\Repo\Entities\Provincia');
    }

}