<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{

    protected $table = 'tareas';

    protected $guarded = ['id'];

    public function prioridad(){
        return $this->belongsTo('App\Repo\Entities\Prioridad');
    }

    public function padre(){
        return $this->belongsTo('App\Repo\Entities\Tarea');
    }

    public function hijas(){
        return $this->hasMany('App\Repo\Entities\Tarea', 'padre_id');
    }

    public function proyecto(){
        return $this->belongsTo('App\Repo\Entities\Proyecto');
    }

    public function parseFecha($fecha){
        return date('dd-mm-YYYY', $fecha);
    }
}