<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{

    protected $table = 'productos';

    protected $guarded = ['id'];

    public function empresa(){
        return $this->hasMany('App\Repo\Entities\Empresa', 'empresa_id');
    }

}