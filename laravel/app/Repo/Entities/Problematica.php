<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class Problematica extends Model
{

    protected $table = 'problematicas';
    protected $guarded = ['id'];
    protected $hidden = ['pivot'];

    public function datos(){
        return $this->hasMany('App\Repo\Entities\Dato', 'problematica_id');
    }

    public function ejes(){
        return $this->belongsToMany('App\Repo\Entities\Eje', 'ejes_problematicas');
    }

    public function proyectos(){
        return $this->belongsToMany('App\Repo\Entities\Proyecto', 'problematicas_proyectos', 'problematica_id', 'proyecto_id');
    }

    public function porques(){
        return $this->hasMany('App\Repo\Entities\Porque', 'problematica_id');
    }

    public function getPorquesInOrder(){
        $porques = [];
        for($x = 0; $x <= 4; $x++){
            $porque = Self::porques()->where('orden', $x)->first();
            if($porque != null){
                $porques[] = [
                    'pregunta' => $porque->pregunta,
                    'respuesta' => $porque->respuesta,
                ];
            } else {
                $porques[] = [
                    'pregunta' => '',
                    'respuesta' => '',
                ];
            }
        }
        return $porques;
    }
}