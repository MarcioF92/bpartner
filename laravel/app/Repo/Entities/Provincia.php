<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{

    protected $table = 'provincias';

    protected $guarded = ['id'];

    public function ciudades(){
        return $this->hasMany('App\Repo\Entities\Ciudad', 'provincia_id');
    }

}