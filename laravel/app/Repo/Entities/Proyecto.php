<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{

    protected $table = 'proyectos';

    protected $guarded = ['id'];

    public function problematicas(){
        return $this->belongsToMany('App\Repo\Entities\Problematica', 'problematicas_proyectos', 'proyecto_id', 'problematica_id');
    }

    public function tareas(){
        return $this->hasMany('App\Repo\Entities\Tarea');
    }

    public function empresa(){
        return $this->belongsTo('App\Repo\Entities\Empresa');
    }

    public function objetivos(){
        return $this->hasMany('App\Repo\Entities\Objetivo');
    }

}