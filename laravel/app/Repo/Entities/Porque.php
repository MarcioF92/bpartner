<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Porque extends Model
{

    protected $table = 'porques';

    protected $guarded = ['id'];

    public function problematica(){
        return $this->belongsTo('App\Repo\Entities\Problematica');
    }
}