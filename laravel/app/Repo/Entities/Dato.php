<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Dato extends Model
{

    protected $table = 'datos_problematica';

    protected $guarded = ['id'];

    public function problematica(){
        return $this->belongsTo('App\Repo\Entities\Problematica');
    }

}