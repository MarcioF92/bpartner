<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Medida extends Model
{

    protected $table = 'medidas';

    protected $guarded = ['id'];

}