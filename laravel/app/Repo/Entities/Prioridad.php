<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Prioridad extends Model
{

    protected $table = 'prioridades';

    protected $guarded = ['id'];

}