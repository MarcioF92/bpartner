<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model
{

    protected $table = 'seguimiento_objetivos';
    protected $guarded = ['id'];
    protected $orderBy;
    protected $orderDirection = 'ASC';

    public function scopeOrdered($query)
    {
        if ($this->orderBy)
        {
            return $query->orderBy($this->orderBy, $this->orderDirection);
        }

        return $query;
    }

    public function scopeGetOrdered($query)
    {
        return $this->scopeOrdered($query)->get();
    }

    public function objetivo(){
        return $this->belongsTo('App\Repo\Entities\Objetivo');
    }

}