<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Eje extends Model
{

    protected $table = 'ejes';
    protected $guarded = ['id'];
    protected $hidden = ['pivot'];

    public function problematicas(){
        return $this->belongsToMany('App\Repo\Entities\Problematica', 'ejes_problematicas');
    }

}