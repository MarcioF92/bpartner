<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Proceso extends Model
{

    protected $table = 'procesos';

    protected $guarded = ['id'];

    public function empresa(){
        return $this->hasMany('App\Repo\Entities\Empresa', 'empresa_id');
    }

}