<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{

    protected $table = 'clientes';

    protected $guarded = ['id'];

    public function empresa(){
        return $this->hasMany('App\Repo\Entities\Empresa', 'empresa_id');
    }

}