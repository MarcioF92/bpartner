<?php

namespace App\Repo\Entities;

use Illuminate\Database\Eloquent\Model;

class Objetivo extends Model
{

    protected $table = 'objetivos_proyecto';
    public $timestamps = false;
    protected $guarded = ['id'];

    public function proyecto(){
        return $this->belongsTo('App\Repo\Entities\Proyecto');
    }

    public function medida(){
        return $this->belongsTo('App\Repo\Entities\Medida');
    }

    public function seguimiento(){
        return $this->hasMany('App\Repo\Entities\Seguimiento', 'objetivo_id');
    }

    public function getTotalSeguimiento(){
        $total = 0;
        foreach ($this->seguimiento as $seguimiento){
            $total += $seguimiento->valor;
        }
        return $total;
    }

}