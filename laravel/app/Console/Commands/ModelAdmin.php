<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ModelAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:modeladmin {--singular_name=} {--extends=} {--plural_name=} {--table=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create model, reader an writer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $args = [
            'singular_name' => $this->option('singular_name'),
            'plural_name' => $this->option('plural_name'),
            'table' => $this->option('table'),
            'extends' => $this->option('extends'),
        ];

        if(empty($this->option('singular_name')) &&
            empty($this->option('plural_name')) &&
            empty($this->option('table')) &&
            empty($this->option('extends'))
        ){
            $this->error('Ingrese todos los parámetros');
            return false;
        }

        try {

            $model = fopen(app_path() . '/Repo/Entities/' . $this->option('singular_name') . '.php', "w");
            fwrite($model, $this->getContent('model', $args));

            $reader = fopen(app_path() . '/Repo/Readers/' . $this->option('plural_name') . 'Reader.php', "w");
            fwrite($reader, $this->getContent('reader', $args));

            $writer = fopen(app_path() . '/Repo/Writers/' . $this->option('plural_name') . 'Writer.php', "w");
            fwrite($writer, $this->getContent('writer', $args));

            $this->info('El modelo ha sido creado correctamente');

        } catch (Exception $e){
            $this->error('Error al crear los archivos '.$e->getMessage());
        }

    }

    public function getContent($type, $args){

        switch ($type){
            case 'model':
                $content = '<?php

namespace App\Repo\Entities;

class '. $args['singular_name'] .' extends '. $args['extends'] .'
{

    protected $table = \''. $args['table'] .'\';

    protected $guarded = [\'id\'];

}';
                return $content;
                break;

            case 'reader':
                $content = '<?php

namespace App\Repo\Readers;

class '. $args['plural_name'] .'Reader extends BaseReader {

    public function __construct($model = \'App\Repo\Entities\\'.$args['singular_name'].'\')
    {
        parent::__construct($model);
    }

}';
                return $content;
                break;

            case 'writer':
                $content = '<?php

namespace App\Repo\Writers;

use App\Repo\Entities\\'. $args['singular_name'] .';

class '. $args['plural_name'] .'Writer extends BaseWriter {

    public function save() {
        \DB::beginTransaction();
        try {
            parent::save();
            \DB::commit();
            return true;
        }catch(\Exception $e) {
            \DB::rollback();
            $this->addError(\'db\', \'No se pudo guardar el registro: \'.$e->getMessage());
            return false;
        }
    }

    public function destroy() {
        return parent::destroy();
    }

}';
                return $content;
                break;
        }

    }
}
