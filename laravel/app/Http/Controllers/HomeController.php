<?php

namespace App\Http\Controllers;

use App\Repo\Readers\EmpresasReader;
use App\Repo\Readers\TareasReader;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = (new EmpresasReader)->all();
        $tareas = (new TareasReader)->all();
        return view('home', compact('empresas', 'tareas'));
    }
}
