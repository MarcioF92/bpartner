<?php

namespace App\Http\Controllers;

use App\Repo\Readers\EmpresasReader;
use App\Repo\Readers\ProductosReader;
use App\Repo\Writers\EmpresasWriter;
use Illuminate\Http\Request;

class EmpresasController extends Controller
{
    protected $reader;

    public function __construct(EmpresasReader $reader)
    {
        $this->reader = $reader;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = $this->reader->all();
        return view('empresas.index', compact('empresas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresa = $this->reader->newItem();
        $provincias = $this->reader->getProvincias();
        return view('empresas.create', compact('empresa', 'provincias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'razon_social' => 'required',
            'ciudad_id' => 'required|exists:ciudades,id',
            'direccion' => 'required',
        ]);

        $empresasWriter = new EmpresasWriter($this->reader->newItem(), $request);
        if(!$empresasWriter->save())
            return redirect()->back()->withInput()->withErrors($empresasWriter->getErrors());
        return redirect()->route('empresas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = $this->reader->findItem($id);
        return view('empresas.show', compact('empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = $this->reader->findItem($id);
        $provincias = $this->reader->getProvincias();
        $ciudades = $this->reader->getCiudades($empresa->ciudad->provincia->id);
        return view('empresas.edit', compact('empresa', 'provincias', 'ciudades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required',
            'razon_social' => 'required',
            'ciudad_id' => 'required|exists:ciudades,id',
            'direccion' => 'required',
        ]);

        $empresasWriter = new EmpresasWriter($this->reader->findItem($id), $request);
        if(!$empresasWriter->save())
            return redirect()->back()->withInput()->withErrors($empresasWriter->getErrors());
        return redirect()->route('empresas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO Hacer Destroy en Cascada
    }

    public function getCiudades(Request $request){
        if (!$request->ajax()) redirect()->to('empresas');

        $ciudades = $this->reader->getCiudades($request->input('id'));

        return response()->json(['ciudades' => $ciudades]);
    }

}
