<?php

namespace App\Http\Controllers;

use App\Repo\Readers\EmpresasReader;
use App\Repo\Readers\ProblematicasReader;
use App\Repo\Readers\ProyectosReader;
use App\Repo\Writers\ProblematicasWriter;
use Illuminate\Http\Request;

class ProblematicasController extends Controller
{
    protected $problematicasReader;
    protected $empresaReader;

    public function __construct(ProblematicasReader $problematicasReader, EmpresasReader $empresaReader)
    {
        $this->problematicasReader = $problematicasReader;
        $this->empresaReader = $empresaReader;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $empresa = $this->empresaReader->findItem($id);
        return view('problematicas.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $empresa = $this->empresaReader->findItem($id);
        $problematica = $this->problematicasReader->newItem();
        $ejes = $this->problematicasReader->getEjes();
        return view('problematicas.create', compact('empresa', 'problematica', 'ejes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'empresa_id' => 'exists:empresas,id',
            'nombre' => 'required',
            'descripcion' => 'required',
            'ejes' => 'array',
            'clave_dato' => 'array',
            'valor_dato' => 'array',
            'ishikawa' => 'file',
        ]);

        $writer = new ProblematicasWriter($this->problematicasReader->newItem(), $request);
        if(!$writer->save())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.problematicas.index', ['id' => $request->input('empresa_id')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $problematica = $this->problematicasReader->findItem($id);
        $empresa = $this->empresaReader->findItem($problematica->empresa_id);
        $ejes = $this->problematicasReader->getEjes();
        return view('problematicas.edit', compact('empresa', 'problematica', 'ejes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'empresa_id' => 'exists:empresas,id',
            'nombre' => 'required',
            'descripcion' => 'required',
            'ejes' => 'array',
            'clave_dato' => 'array',
            'valor_dato' => 'array',
            'ishikawa' => 'file',
        ]);

        $writer = new ProblematicasWriter($this->problematicasReader->findItem($id), $request);
        if(!$writer->save())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.problematicas.index', ['id' => $request->input('empresa_id')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = request();
        if (!$request->ajax()) redirect()->to('/');
        $problematica = $this->problematicasReader->findItem($id);
        if($problematica == null)
            return response()->json('La problemática no existe', 418, [], JSON_UNESCAPED_UNICODE);
        $problematicaWriter = new ProblematicasWriter($problematica, $request);
        if(!$problematicaWriter->destroy())
            return response()->json('No se ha podido borrar el proceso '.$problematicaWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true], 200);
    }
}
