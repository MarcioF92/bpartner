<?php

namespace App\Http\Controllers;

use App\Repo\Readers\EmpresasReader;
use App\Repo\Readers\ProcesosReader;
use App\Repo\Writers\ProcesosWriter;
use Illuminate\Http\Request;

class ProcesosController extends Controller
{
    protected $procesoReader;
    protected $empresaReader;


    public function __construct(ProcesosReader $procesoReader, EmpresasReader $empresaReader)
    {
        $this->procesoReader = $procesoReader;
        $this->empresaReader = $empresaReader;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $empresa = $this->empresaReader->findItem($id);
        return view('procesos.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id){
        $empresa = $this->empresaReader->findItem($id);
        $proceso = $this->procesoReader->newItem();
        return view('procesos.create', compact('empresa', 'proceso'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $writer = new ProcesosWriter($this->procesoReader->newItem(), $request);
        $response = $writer->save();
        if(!$response['success'])
            return response()->json('No se ha podido guardar el proceso '.$procesoWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true, 'action' => 'refresh'], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proceso = $this->procesoReader->findItem($id);
        $empresa = $this->empresaReader->findItem($proceso->empresa_id);

        return view('procesos.edit', compact('empresa', 'proceso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $writer = new ProcesosWriter($this->procesoReader->findItem($id), $request);
        $response = $writer->save();
        if(!$response['success'])
            return response()->json('No se ha podido editar el proceso '.$procesoWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true, 'action' => 'toast'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = request();
        if (!$request->ajax()) redirect()->to('/');
        $proceso = $this->procesoReader->findItem($id);
        if($proceso == null)
            return response()->json('El proceso no existe', 418, [], JSON_UNESCAPED_UNICODE);
        $procesoWriter = new ProcesosWriter($proceso, $request);
        if(!$procesoWriter->destroy())
            return response()->json('No se ha podido borrar el proceso '.$procesoWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true], 200);
    }
}
