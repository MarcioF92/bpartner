<?php

namespace App\Http\Controllers;

use App\Repo\Readers\EmpresasReader;
use App\Repo\Readers\ClientesReader;
use App\Repo\Writers\ClientesWriter;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    protected $clienteReader;
    protected $empresaReader;


    public function __construct(ClientesReader $clienteReader, EmpresasReader $empresaReader)
    {
        $this->clienteReader = $clienteReader;
        $this->empresaReader = $empresaReader;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $empresa = $this->empresaReader->findItem($id);
        return view('clientes.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id){
        $empresa = $this->empresaReader->findItem($id);
        $cliente = $this->clienteReader->newItem();
        return view('clientes.create', compact('empresa', 'cliente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $writer = new ClientesWriter($this->clienteReader->newItem(), $request);
        if(!$writer->save())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.clientes.index', $request->input('empresa_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = $this->clienteReader->findItem($id);
        $empresa = $this->empresaReader->findItem($cliente->empresa_id);

        return view('clientes.edit', compact('empresa', 'cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $writer = new ClientesWriter($this->clienteReader->findItem($id), $request);
        if(!$writer->save())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.clientes.index', $request->input('empresa_id'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = request();
        if(!$request->ajax()) return redirect('/');

        $cliente = $this->clienteReader->findItem($id);
        if($cliente == null)
            return response()->json('El cliente no existe', '418');

        $writer = new ClientesWriter($cliente, $request);
        if(!$writer->destroy())
            return response()->json('No se pudo borrar el registro', '418');
        return response()->json(['success' => true]);
    }
}
