<?php

namespace App\Http\Controllers;

use App\Repo\Entities\Objetivo;
use App\Repo\Entities\Tarea;
use App\Repo\Readers\EmpresasReader;
use App\Repo\Readers\ProyectosReader;
use App\Repo\Readers\TareasReader;
use App\Repo\Writers\ProyectosWriter;
use App\Repo\Writers\TareasWriter;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    protected $proyectosReader;
    protected $empresaReader;

    public function __construct(ProyectosReader $proyectosReader, EmpresasReader $empresaReader)
    {
        $this->proyectosReader = $proyectosReader;
        $this->empresaReader = $empresaReader;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $empresa = $this->empresaReader->findItem($id);
        return view('proyectos.index', compact('empresa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $empresa = $this->empresaReader->findItem($id);
        $proyecto = $this->proyectosReader->newItem();
        $medidas = $this->proyectosReader->getMedidas();
        return view('proyectos.create', compact('empresa', 'proyecto', 'medidas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'empresa_id' => 'exists:empresas,id',
            'nombre' => 'required',
            'descripcion' => 'required',
            'problematicas' => 'array',
            'clave_objetivo' => 'array',
            'valor_objetivo' => 'array',
            'medida_objetivo' => 'array',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->newItem(), $request);
        if(!$writer->save())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.index', ['id' => $request->input('empresa_id')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proyecto = $this->proyectosReader->findItem($id);
        $empresa = $this->empresaReader->findItem($proyecto->empresa_id);
        return view('proyectos.edit', compact('empresa', 'proyecto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'empresa_id' => 'exists:empresas,id',
            'nombre' => 'required',
            'descripcion' => 'required',
            'problematicas' => 'array',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($id), $request);
        if(!$writer->save())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.index', ['id' => $request->input('empresa_id')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request = request();
        if (!$request->ajax()) redirect()->to('/');
        $proyecto = $this->proyectosReader->findItem($id);
        if($proyecto == null)
            return response()->json('El proyecto no existe', 418, [], JSON_UNESCAPED_UNICODE);
        $proyectoWriter = new ProyectosWriter($proyecto, $request);
        if(!$proyectoWriter->destroy())
            return response()->json('No se ha podido borrar el proceso '.$proyectoWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true], 200);
    }

    /**
     * Tareas
     */
    public function tareas($id){
        $proyecto = $this->proyectosReader->findItem($id);
        return view('proyectos.tareas.index', compact('proyecto'));
    }

    public function agregarTarea($id){
        $proyecto = $this->proyectosReader->findItem($id);
        $tareasReader = new TareasReader();
        $tarea = $tareasReader->newItem();
        $tareas = $proyecto->tareas;
        $prioridades = $tareasReader->getPrioridades();
        $fechaInicio = date('d-m-Y');
        $fechaFin = date('d-m-Y');
        return view('proyectos.tareas.create', compact('proyecto', 'tarea', 'prioridades', 'tareas', 'fechaInicio', 'fechaFin'));
    }

    public function guardarTarea(Request $request){
        $request->validate([
            'proyecto_id' => 'exists:proyectos,id',
            'nombre' => 'required',
            'descripcion' => 'required',
            'fecha_comienzo' => 'required',
            'fecha_fin' => 'required',
            'prioridad' => 'exists:prioridades,id',
            'padre' => 'nullable|exists:tareas,id',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($request->input('proyecto_id')), $request);
        if(!$writer->guardarTarea())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.tareas.index', ['id' => $request->input('proyecto_id')]);
    }

    public function verTarea($id){
        $tarea = (new TareasReader)->findItem($id);
        return view('proyectos.tareas.show', compact('tarea'));
    }

    public function editarTarea($id){
        $tareasReader = new TareasReader();
        $tarea = $tareasReader->findItem($id);
        $tareas = $tareasReader->findBy('id', $id, '<>', true);
        $prioridades = $tareasReader->getPrioridades();
        $proyecto = $this->proyectosReader->findItem($tarea->proyecto_id);
        $fechaInicio = date('d-m-Y', strtotime($tarea->fecha_comienzo));
        $fechaFin = date('d-m-Y', strtotime($tarea->fecha_fin));
        return view('proyectos.tareas.edit', compact('proyecto', 'tarea', 'prioridades', 'tareas', 'fechaInicio', 'fechaFin'));
    }

    public function actualizarTarea(Request $request, $id){
        $request->validate([
            'proyecto_id' => 'exists:proyectos,id',
            'nombre' => 'required',
            'descripcion' => 'required',
            'fecha_comienzo' => 'required',
            'fecha_fin' => 'required',
            'prioridad' => 'exists:prioridades,id',
            'padre' => 'nullable|exists:tareas,id',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($request->input('proyecto_id')), $request);
        if(!$writer->guardarTarea($id))
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.tareas.index', ['id' => $request->input('proyecto_id')]);
    }

    public function eliminarTarea($id){
        $request = request();
        if (!$request->ajax()) redirect()->to('/');
        $tarea = (new TareasReader)->findItem($id);
        if($tarea == null)
            return response()->json('La tarea no existe', 418, [], JSON_UNESCAPED_UNICODE);
        $proyectoWriter = new ProyectosWriter($tarea->proyecto, $request);
        if(!$proyectoWriter->eliminarTarea($id))
            return response()->json('No se ha podido borrar el proceso '.$proyectoWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true], 200);
    }

    /* Objetivos de proyecto */
    public function agregarObjetivo($id){
        $proyecto = $this->proyectosReader->findItem($id);
        $objetivo = new Objetivo();
        $medidas = $this->proyectosReader->getMedidas();
        return view('proyectos.seguimiento.create-objetivo', compact('proyecto', 'objetivo', 'medidas'));
    }

    public function guardarObjetivo(Request $request){
        $request->validate([
            'clave' => 'required',
            'valor' => 'required',
            'medida' => 'exists:medidas,id',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($request->input('proyecto_id')), $request);
        if(!$writer->guardarObjetivo())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.objetivos.seguimiento.index', ['id' => $request->input('proyecto_id')]);
    }

    public function editarObjetivo($id){
        $objetivo = $this->proyectosReader->getObjetivo($id);
        $proyecto = $this->proyectosReader->findItem($objetivo->proyecto_id);
        $medidas = $this->proyectosReader->getMedidas();
        return view('proyectos.seguimiento.edit-objetivo', compact('proyecto', 'objetivo', 'medidas'));
    }

    public function actualizarObjetivo(Request $request, $id){
        $request->validate([
            'clave' => 'required',
            'valor' => 'required',
            'medida' => 'exists:medidas,id',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($request->input('proyecto_id')), $request);
        if(!$writer->guardarObjetivo($id))
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.objetivos.seguimiento.index', ['id' => $request->input('proyecto_id')]);
    }

    public function eliminarObjetivo($id){
        $request = request();
        if (!$request->ajax()) redirect()->to('/');
        $objetivo = $this->proyectosReader->getObjetivo($id);
        if($objetivo == null)
            return response()->json('El objetivo no existe', 418, [], JSON_UNESCAPED_UNICODE);
        $writer = new ProyectosWriter($objetivo->proyecto, $request);
        if(!$writer->eliminarObjetivo($id))
            return response()->json('No se ha podido borrar el proceso '.$writer->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true], 200);
    }

    /* Seguimiento de proyecto */
    public function seguimiento($id){
        $proyecto = $this->proyectosReader->findItem($id);
        return view('proyectos.seguimiento.index', compact('proyecto'));
    }

    public function agregarSeguimiento($id){
        $objetivo = $this->proyectosReader->getObjetivo($id);
        $seguimiento = $this->proyectosReader->newSeguimiento($id);
        return view('proyectos.seguimiento.create', compact('objetivo', 'seguimiento'));
    }

    public function guardarSeguimiento(Request $request){
        $request->validate([
            'objetivo_id' => 'required|exists:objetivos_proyecto,id',
            'valor' => 'required',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($request->input('proyecto_id')), $request);

        if(!$writer->guardarSeguimiento())
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.objetivos.seguimiento.index', ['id' => $request->input('proyecto_id')]);
    }

    public function editarSeguimiento($id){
        $seguimiento = $this->proyectosReader->getSeguimiento($id);
        $objetivo = $seguimiento->objetivo;
        return view('proyectos.seguimiento.edit', compact('objetivo', 'seguimiento'));
    }

    public function actualizarSeguimiento(Request $request, $id){
        $request->validate([
            'objetivo_id' => 'required|exists:objetivos_proyecto,id',
            'valor' => 'required',
        ]);

        $writer = new ProyectosWriter($this->proyectosReader->findItem($request->input('proyecto_id')), $request);

        if(!$writer->guardarSeguimiento($id))
            return redirect()->back()->withInput()->withErrors($writer->getErrors());
        return redirect()->route('empresas.proyectos.objetivos.seguimiento.index', ['id' => $request->input('proyecto_id')]);
    }

    public function eliminarSeguimiento($id){
        $request = request();
        if (!$request->ajax()) redirect()->to('/');
        $seguimiento = $this->proyectosReader->getSeguimiento($id);
        if($seguimiento == null)
            return response()->json('El seguimiento no existe', 418, [], JSON_UNESCAPED_UNICODE);
        $proyectoWriter = new ProyectosWriter($seguimiento->objetivo->proyecto, $request);
        if(!$proyectoWriter->eliminarSeguimiento($id))
            return response()->json('No se ha podido borrar el seguimiento '.$proyectoWriter->getErrors(), 418, [], JSON_UNESCAPED_UNICODE);
        return response()->json(['success' => true], 200);
    }


}
