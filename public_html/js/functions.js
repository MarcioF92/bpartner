$(document).ready(function(){

    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var baseUrl = $('meta[name="base-url"]').attr('content');

    function eliminarRegistro(urlDelete) {
        var jqxhr = $.ajax({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            },
            url: urlDelete,
            method: 'DELETE',
        });
        return jqxhr;
    }

    $(".remove").on('click', function(e){
        e.preventDefault();
        var button = $(this)
        if(button.data('type') == 'tarea'){
            var proyecto = button.data('proyecto');
        } else {
            var proyecto = false;
        }
        console.log(proyecto);
        $.confirm({
            title: 'Se requiere confirmación',
            content: '¿Está seguro que desea eliminar el registro?',
            buttons: {
                confirm: {
                    text: 'Sí',
                    action: function() {
                        if(button.data('type') == 'tarea'){
                            var proyecto = button.data('proyecto');
                        } else {
                            var proyecto = false;
                        }
                        var jqxhr = eliminarRegistro(button.attr('href'));
                        jqxhr.done(function (res) {
                            if(res.success){
                                if(proyecto === false) {
                                    document.location.href = document.location.href;
                                } else {
                                    document.location.href = baseUrl + '/empresas/proyectos/'+proyecto+'/tareas';
                                }
                            } else {
                                console.log(res.msj)
                            }
                        }).fail(function (response, error, message) {
                            console.log(response.responseText, true);
                        });
                    }
                },
                cancel: {
                    text: 'No'
                }
            }
        });


    });

    var empresas = $("#empresas");
    if(empresas.length){
        empresas.on('change', '#provincia-selector', function(e){
            var jqxhr = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                url: baseUrl + '/empresas/get_ciudades',
                method: 'POST',
                data:{
                    id: $(this).val()
                },
            });

            jqxhr.done(function (res) {
                if(res.ciudades){
                    var selectCiudades = empresas.find('#ciudad-selector');
                    selectCiudades.html('<option selected="selected" value="">Ciudad</option>');
                    $.each(res.ciudades, function (index, ciudad) {
                        selectCiudades.append($('<option>', {
                            value: index,
                            text: ciudad
                        }));
                        selectCiudades.removeAttr('disabled');
                    });

                    selectCiudades.material_select();

                } else {
                    console.log(res.msj)
                }
            }).fail(function (response, error, message) {
                console.log(response.responseText, true);
            });
        });
    }

    var problematicas = $("#problematicas");
    if(problematicas.length){
        problematicas.on('click', '#agregar-caracteristica', function(){
            console.log("Click");
            var lista = problematicas.find('#lista-caracteristicas');
            lista.append('<div class="row"><div class="input-field col s5"><input type="text" name="clave_dato[]" class="validate" /><label for="clave_dato">Nombre</label></div><div class="input-field col s5"><input type="text" name="valor_dato[]" class="validate" /><label for="valor_dato">Valor</label></div><div class="col s2"><a class="waves-effect waves-light eliminar-caracteristica"><i class="material-icons">delete</i></a></div></div>');
        });
        problematicas.on('click', '.eliminar-caracteristica', function () {
            $(this).parent().parent().remove();
        })
    }

});