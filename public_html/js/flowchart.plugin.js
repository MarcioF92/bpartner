$(document).ready(function() {

    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var baseUrl = $('meta[name="base-url"]').attr('content');

    var procesos = $("#procesos");
    if (procesos.length) {
        var data = {};

        // Apply the plugin on a standard, empty div...
        var flowchart = $('#flowchart');
        flowchart.flowchart({
            data: data
        });

        var actions = $("#flowchart-actions");

        actions.on('click', '.delete_selected_button', function(e) {
            e.preventDefault();
            flowchart.flowchart('deleteSelected');
        });

        $(document).on('keyup', function(e){
            e.preventDefault();
            if(e.keyCode == 46){
                flowchart.flowchart('deleteSelected');
            }
        });

        var operatorI = 0;
        actions.on('click', '.create_operator', function(e) {
            e.preventDefault();

            var nombreOperador = $(".nuevo_operador_nombre").val();
            var tipoOperador = $('input[name=tipo_operador]:checked').val();

            var operatorId = 'created_operator_' + operatorI;
            var operatorData = {
                top: 60,
                left: 60,
                properties: {
                    title: nombreOperador,
                    inputs: {},
                    outputs: {}
                }
            };

            switch (tipoOperador){
                case 'inicio':
                    operatorData['properties']['outputs'] = {
                        output_1: {
                            label: 'Salida',
                        }
                    }
                    break;

                case 'final':
                    operatorData['properties']['inputs'] = {
                        input_1: {
                            label: 'Entrada',
                        }
                    }
                    break;

                case 'accion':
                    operatorData['properties']['inputs'] = {
                        input_1: {
                            label: 'Entrada',
                        }
                    }
                    operatorData['properties']['outputs'] = {
                        output_1: {
                            label: 'Salida',
                        }
                    }
                    break;

                case 'decision':
                    operatorData['properties']['inputs'] = {
                        input_1: {
                            label: '',
                        }
                    }
                    operatorData['properties']['outputs'] = {
                        verdadero: {
                            label: 'Verdadero',
                        },
                        falso: {
                            label: 'Falso',
                        }
                    }
                    break;
            }

            operatorI++;

            flowchart.flowchart('createOperator', operatorId, operatorData);

            $(".nuevo_operador_nombre").val('');

            $(".show-code").click(function(e){
                var data = flowchart.flowchart('getData');
                $(".code").html(JSON.stringify(data, null, 2));
            });
        });

        $("form").on('submit', function(e){
            e.preventDefault();
            e.stopPropagation();

            var data = flowchart.flowchart('getData');
            var proceso = JSON.stringify(data, null, 2);

            var jqxhr = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                url: '/empresas/procesos/store',
                method: 'POST',
                data:{
                    nombre: $('.nombre_proceso').val(),
                    descripcion: $('.descripcion_proceso').val(),
                    empresa_id: $('.empresa_id').val(),
                    proceso: proceso
                },
            });

            jqxhr.done(function (res) {
                if(res.success){
                    document.location.href = __root__ + '/empresas/'+$('.empresa_id').val()+'/procesos';
                } else {
                    console.log(res.msj)
                }
            }).fail(function (response, error, message) {
                console.log(response.responseText, true);
            });
        })
    }
});
